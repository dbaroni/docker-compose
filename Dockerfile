FROM php:8-fpm
RUN apt-get update && apt-get install -y libonig-dev git zip unzip
RUN docker-php-ext-install pdo_mysql mbstring
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN mkdir /app
VOLUME /app
WORKDIR /app
EXPOSE 9000
CMD php -S 0.0.0.0:9000 -t public