<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/hello', function () {
    return 'Hello World';
});

$router->get('/phpinfo', function () {
    return phpinfo();
});


$router->get('/test-db', function () {
    dd(DB::select("SHOW DATABASES"));
});

//ritorna una risposta http
return new response(null, 200);
}

/** questo metodo aggiorna un record todo nella tabella todos
 * questa viene chiamata con /api/todos/{id} [PUT] * 
 * 
 */
public function update(request $request,$id)
{
/* creiamo le variabili nella tabella todos */
$description = $request->input('description');
$done = $request->input('done');
$expireDate = $request->input('expireDate');

if ($expireDate)





};
